package com.kreezcraft.burninthesun;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = BurnInTheSun.MODID, name = BurnInTheSun.NAME, version = BurnInTheSun.VERSION, acceptableRemoteVersions = "*")
public class BurnInTheSun {
	public static final String MODID = "burninthesun";
	public static final String NAME = "Burn In The Sun";
	public static final String VERSION = "@version@";

	@Mod.Instance
	public static BurnInTheSun instance;

	public static Logger logger;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger = event.getModLog();
	}
}
