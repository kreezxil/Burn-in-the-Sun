package com.kreezcraft.burninthesun;

import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.util.Arrays;

@Mod.EventBusSubscriber
public class DamnSun {

	@SubscribeEvent
	public static void entityUpdate(LivingUpdateEvent event) {
		if (event.getEntity() instanceof EntityPlayer) return;
		final EntityLivingBase entityLiving = event.getEntityLiving();
		final World world = entityLiving.getEntityWorld();
		if (!world.isRemote) {
			String fullId = EntityList.getKey(entityLiving).toString();
			String[] id = fullId.split(":");
			BlockPos blockpos = event.getEntity().getPosition();

			// If the setting is true, burn everything and forget the rest of the method
			if (Settings.tanningLotionSettings.tanninglotion || Arrays.asList(Settings.tanningLotionSettings.mobList).contains(fullId) ||
					Arrays.asList(Settings.tanningLotionSettings.modList).contains(id[0])) {
				if (world.canSeeSky(blockpos) && world.isDaytime() && !entityLiving.isBurning() && !entityLiving.isInWater() && !world.isRainingAt(blockpos)) {
					//if(Settings.tanningLotionSettings.debugMode) System.out.println(entity.getDisplayName().toString()+" should be burning!");
					entityLiving.setFire(1);
				}
			}
		}
	}

	@SubscribeEvent
	public static void playerUpdate(TickEvent.PlayerTickEvent event) {
		EntityPlayer player = event.player;
		if (player == null) return;

		World world = player.getEntityWorld();
		if (!world.isRemote) {
			if (player.isCreative()) return;
			if (Settings.tanningLotionSettings.opNoburn) {
				// is the player in creative or an operator?
				MinecraftServer server = player.getServer();
				// is it a real server?

				if (server != null && !server.isSinglePlayer() && player != null) {

					if (server.getPlayerList().getOppedPlayers().getGameProfileFromName(player.getName()) != null) {
						// player is op
						return;
					}

				}
			}
			BlockPos blockpos = player.getPosition();
			String debugMsg = "";
			//if(!day && event.getEntityPlayer().isBurning() && !event.getEntityPlayer().isInLava()) {
			//debugMsg += "SkylightSubtracted: " + world.getSkylightSubtracted() + ", ";
			if (!world.isDaytime()) {
				return;
			}
			// If the setting is true, burn everything and forget the rest of the method
			if (Settings.tanningLotionSettings.tanninglotion || Settings.tanningLotionSettings.burnPlayers) {
				debugMsg += "tanninglotion or burnplayers is enabled, ";
				if (world.canSeeSky(blockpos)) {
					debugMsg += "player can see sky, ";
					if (world.isDaytime() && !world.isRainingAt(blockpos)) {
						debugMsg += "is day, not raining, ";
						if (!player.isBurning() && !player.isInWater()) {
							debugMsg += "not burning, not in water.";
							player.setFire(1);
						}
					}
				}
			}
			//System.out.println(debugMsg);
		}
	}


}
