package com.kreezcraft.burninthesun;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber
@Config(modid = BurnInTheSun.MODID, category = "")
public class Settings {
	
	@Config.Comment({ "Oh, my! What besides Skeles and Zombos should also burned in the sun!" })
	@Config.Name("Tanning Lotion Application Center")
	public static TanningLotionSettings tanningLotionSettings = new TanningLotionSettings();

	public static class TanningLotionSettings {
		
		@Config.Comment({"Super Global. If enabled the other settings don't even matter for obvious reasons. Do not enable, every mob including players will burn in the sun. Default: false"})
		@Config.Name("TanningLotion")
		public boolean tanninglotion = false;
		
		@Config.Comment({"A global setting. If you specify a mod here, you don't have to specify individual mobs later. Default: deadlymonsters"})
		@Config.Name("ModList")
		public String[] modList = new String[]{"dmonsters"};
		
		@Config.Comment({"Specific mobs that should have tanninglotion applied. If the mod the mob belongs to is specified above this will be pointless. The format is modid:mobname all lowercase. Default: minecraft:skeleton, minecraft:zombie"})
		@Config.Name("MobList")
		public String[] mobList = new String[]{"minecraft:chicken"};

		@Config.Comment({"Should players burn? Don't ask for specific players to burn. I won't do that."})
		@Config.Name("burnPlayers")
		public boolean burnPlayers = false;

		@Config.Comment({"Should ops not burn?"})
		@Config.Name("opNoburn")
		public boolean opNoburn=true;
	}

	@SubscribeEvent
	public static void onConfigChanged(ConfigChangedEvent event) {
		if (event.getModID().equals(BurnInTheSun.MODID)) {
			ConfigManager.sync(BurnInTheSun.MODID, Config.Type.INSTANCE);
		}
	}

}
