Normally, only skeletons, zombies, and other mobs will burn in the sun. With this mod installed and if you don't change the config, chickens will too! Yes, I enabled them in the config, so you can see how to do it.

**This mod requires configuration.**

You can set players, any mob, or the mobs from an entire mod to burn in the sun.

There's even a super global setting if enabled makes every living thing burn in the sun!

**Can be run server side only.**

**See the sister mod **

[https://www.curseforge.com/minecraft/mc-mods/mob-sunscreen](https://www.curseforge.com/minecraft/mc-mods/mob-sunscreen)

## Other Projects

[Active Projects](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fkreezcraft.com%252factive-projects%252f)
